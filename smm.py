import argparse
import json
import smtplib
import getpass
import os
import pathlib

from collections import deque
from email.mime.text import MIMEText
from time import sleep

import requests
import pytz
from dateutil import parser as date_parser
from lxml import html

search_for = []
flair_filter = []
from_email = ''
smtp_server = ''
to_email = ''
password = ''

dirname = os.path.dirname(__file__)
fname = os.path.join(dirname, 'smm_links.txt')
cfgfile = os.path.join(dirname, 'smm.cfg')
base_url = 'https://old.reddit.com'
repeat_period = 30  # seconds

MAX_LINKS_IN_FILE = 1024


def read_config(f):
    with open(f, 'r') as fh:
        return json.load(fh)


def get_password(cfg):
    if not cfg['email_pass']:
        return getpass.getpass(f"Password for {cfg['from_email']}: ")
    return cfg['email_pass']


def get_saved_hits(args):
    hits_file = pathlib.Path(args.out_file)
    hits_file.touch(exist_ok=True)

    if hits_file.stat().st_size == 0:
        return deque()

    with open(hits_file, 'r') as f:
        saved_hits = json.load(f)

    return deque(saved_hits)


def main(args):
    cfg = read_config(args.config)
    search_for = cfg['terms']
    flair_filter = cfg['flair']
    flair_filter.append(
        '')  # sometimes a post is made without a flair so appending an empty string to the filter ensures we search those posts as well
    from_email = cfg['from']
    to_email = ','.join(cfg['to'])
    smtp_server = cfg['smtp_server']

    password = get_password(cfg)

    base_sub = cfg['subreddit']
    subject = cfg['subject']
    # get a list of previously found links so we don't grab them again
    previous_links = get_saved_hits(args)
    # with open(fname, 'r') as f:
    #     previous_links = f.readlines()
    # previous_links = deque([x.strip() for x in previous_links])
    while True:
        # download the page
        try:
            page = requests.get(base_url + base_sub, headers={'User-agent': 'Mozilla/5.0'})
        except Exception as e:
            # print(str(e))
            sleep(repeat_period)
            continue

        # scrub the html for links, this part is likely to break if reddit changes their layout again.
        dom = html.fromstring(page.text)
        links = dom.find_class('title may-blank')
        previous_hrefs = [hit['href'] for hit in previous_links]
        hrefs = []
        for link in links:
            href = link.get('href')
            # print(href)
            # match terms
            if any(term.lower() in link.text.lower() for term in search_for) and href not in previous_hrefs:
                try:
                    timestamp = date_parser.parse(
                        link.getparent().getparent().find_class('live-timestamp')[0].get('datetime')).astimezone(
                        pytz.timezone(cfg['timezone']))
                    print(timestamp)
                except IndexError:
                    continue
                flair = link.getparent().find_class('linkflairlabel')
                comments = link.getparent().getparent().find_class('first')[0][0].get('href')
                if len(flair) > 0:
                    flair = flair[0].get('title')
                else:
                    flair = 'None'

                # match flair
                if len(flair_filter) == 0 or flair in flair_filter:
                    print("Added href")
                    # hrefs.append((flair + ' ' + link.text, base_url + href, timestamp))
                    hit = {
                        "flair": flair,
                        "title": link.text,
                        "comments": comments,
                        "href": href,
                        "posted_date": timestamp.strftime('%Y-%m-%d %H:%M:%S')
                    }
                    hrefs.append(hit)
                    previous_links.appendleft(hit)

        # if we found more than 0 links we will send out an email.
        if len(hrefs) > 0:
            message_body = ''
            # compile found links into an email body
            for href in hrefs:
                # print("{}\n{}\n{}\n\n".format(href[0], href[1], str(href[2])))
                message_body = message_body + href['title'] + '\r\n\r\n' + href['href'] + '\r\n\r\n' + str(
                    href['comments']) + '\r\n\r\n' + href['posted_date'] + '\r\n\r\n\r\n\r\n'

            message = MIMEText(message_body.encode('utf-8'), _charset='utf-8')
            message['From'] = from_email
            message['To'] = to_email
            message['Subject'] = subject
            mailer = smtplib.SMTP(smtp_server)
            mailer.ehlo()
            mailer.starttls()
            mailer.login(from_email, password)
            mailer.sendmail(from_email, to_email, message.as_string())

        # re-write the previous links file
        if len(previous_links) > MAX_LINKS_IN_FILE:
            for i in range(len(previous_links) - MAX_LINKS_IN_FILE):
                previous_links.pop()

        with open(args.out_file, 'w+') as f:
            json.dump(list(previous_links), f, indent=2)
        # linkFiles = open(fname, 'w+')
        # for link in previous_links:
        #     linkFiles.write("{}\n".format(link))
        # linkFiles.close()

        sleep(repeat_period)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default="smm.cfg", help="Config file path.")
    parser.add_argument("--out-file", default="smm_links.txt", help="File to save hits to.")
    main(parser.parse_args())
